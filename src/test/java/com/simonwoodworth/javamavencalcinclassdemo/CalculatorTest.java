/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.javamavencalcinclassdemo;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author simon
 */
public class CalculatorTest {
    
   @Test
    public void testAdd() {
        Calculator c = new Calculator();
        assertEquals(12,c.add(7, 5));
    }
    
    @Test
    public void testMultiply() {
        Calculator c = new Calculator();
        assertEquals(35,c.multiply(7, 5));
    }
    
    @Test
    public void testSubtract() {
        Calculator c = new Calculator();
        assertEquals(2,c.subtract(7, 5));
    }
    
    @Test
    public void testDivide() {
        Calculator c = new Calculator();
        assertEquals(1,c.divide(7, 5));
    }
    
    @Test
    public void testSquare() {
        Calculator c = new Calculator();
        assertEquals(64,c.square(64));
    }

    @Test
    public void testCube() {
        Calculator c = new Calculator();
        assertEquals(27,c.cube(27));
    }
    
}
